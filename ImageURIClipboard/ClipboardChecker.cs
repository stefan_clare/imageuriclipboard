﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ImageURIClipboard
{
    public class ClipboardChecker
    {
        private Thread _thread = null;


        public ClipboardChecker()
        {
        }

        public void StartChecker()
        {
            if (_thread != null && _thread.IsAlive)
            {
                _thread.Abort();
                _thread = null;
            }

            _thread = new Thread(
                () =>
                {
                    try
                    {
                        while (true)
                        {
                            var t = new Thread(() => CheckClipboard());
                            t.SetApartmentState(ApartmentState.STA);
                            t.Start();
                            t.Join();
                            Thread.Sleep(1000);
                        }
                    }
                    catch (ThreadAbortException)
                    {
                    }
                });
            _thread.Start();
        }

        public void StopChecker()
        {
            _thread?.Abort();
            if (_thread != null) _thread = null;
        }

        private void CheckClipboard()
        {
            if (Clipboard.ContainsText(TextDataFormat.UnicodeText))
            {
                string data = Clipboard.GetText();
                if (data.StartsWith("data:image/png;base64"))
                {
                    NewElementFoundEvent?.Invoke(this, data);
                    Clipboard.Clear();
                }
            }
        }


        public event EventHandler<String> NewElementFoundEvent;
    }
}
