﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageURIClipboard
{
    class Program
    {
        static void Main(string[] args)
        {

            ClipboardChecker checker = new ClipboardChecker();

            checker.NewElementFoundEvent += Checker_NewElementFoundEvent;

            checker.StartChecker();

            Console.WriteLine("Any Key to close ...");
            Console.ReadKey();

            checker.StopChecker();
        }

        private static void Checker_NewElementFoundEvent(object sender, string e)
        {
            SimpleImageGenerator.CreateImageFile(e);
            GC.Collect();
        }
    }
}
