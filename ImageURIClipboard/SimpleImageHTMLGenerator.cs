﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageURIClipboard
{
    public class SimpleImageHTMLGenerator
    {
        private const String ImageControlTemplate = "<img src=\"{0}\" />";

        private static readonly String HtmlTemplate = 
            $"<html><head></head><body>{ImageControlTemplate}<body><html>";

        public static void CreateImageHtmlFile(string ImageData)
        {
            var now = DateTime.Now;
            var filedata = String.Format(HtmlTemplate, ImageData);
            var filename = $"Image_{now:yyyy_M_dd_HH_mm_ss}.html";
            File.WriteAllText(filename, filedata);
        }

    }
}
