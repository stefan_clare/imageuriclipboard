﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImageURIClipboard
{
    public static class SimpleImageGenerator
    {
        public static void CreateImageFile(string dataURI)
        {
            var dataMatch = Regex.Match(dataURI, @"data:image/(?<type>.+?);(?<database>.+?),(?<data>.+)");
            var base64Data = dataMatch.Groups["data"].Value;
            var typeData = dataMatch.Groups["type"].Value;
            var binData = Convert.FromBase64String(base64Data);

            var now = DateTime.Now;
            var filename = $"Image_{now:yyyy_M_dd_HH_mm_ss}.{typeData}";
            if (!File.Exists(filename))
                File.WriteAllBytes(filename, binData);
        }
    }
}
